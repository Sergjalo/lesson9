package by.EPAM.trainJava;

public class UseCustomList {
 public static void main(String[] args) {
	 CustomLinkList <Integer> ls = new CustomLinkList <Integer>();
	 // начнем добавлять сначала
	 ls.addToHead(3);
	 ls.addToHead(8);
	 ls.addToTail(2);
	 System.out.println(ls);
	 // отчистим
	 System.out.println(ls.clear());
	 
	 // начнем добавлять с конца
	 ls.addToTail(2);
	 ls.addToHead(3);
	 ls.addToHead(8);
	 System.out.println(ls);
 }
}
