package by.EPAM.trainJava;

public class CustomLinkList<T> {
	private PointLink <T> pointHead;
	private PointLink <T> pointTail;
	private int size;
	
	/***
	 * конструкторы ------------------------------
	 */
	public CustomLinkList () {
		this.pointHead = null;//new PointLink<T>(null, null);
		this.pointTail = pointHead;
		this.size = 0;
	}

	public CustomLinkList(T el) {
		this.pointHead = new PointLink<T>(el, null);
		this.pointTail = pointHead;
		this.size = 1;
	}
	//конструкторы ------------------------------
	
	public boolean addToHead (T el) {
		try {
			PointLink<T> r=new PointLink<T>(el, pointHead);
			pointHead=r;
			size++;
			if (size==1) {
				pointTail=pointHead;
			}
		} catch (Throwable e) {
			return false;
		}		
		return true;
	}
	
	public boolean addToTail (T el) {
		try {
			pointTail.lnk=new PointLink<T>(el, null);
			pointTail=pointTail.lnk;
			size++;
			if (size==1) {
				pointHead=pointTail;
			}
		} catch (Throwable e) {
			return false;
		}		
		return true;
	}
	//---------------------------------------------
	 
	public boolean delHead () {
		try {
			pointHead=pointHead.lnk;
			size--;
		} catch (Throwable e) {
			return false;
		}		
		return true;
	}
	
	/***
	 * перейдем на указанный элемент списка и вернем ссылку на него
	 * @param steps 0-вернуть голову Ышяу - вернуть хвост
	 * @return ссцлка на найденный элемент
	 */
	private PointLink<T> traversToLast(int steps){
		if (steps>size){
			return null;
		}
		PointLink<T> l=pointHead;
		int i=0;
		while ((i<steps)&&(l!=pointTail)) {
			l=l.lnk;
			i++;
		}
		return l;
	}
	
	/***
	 * удалим указанный iй элемент
	 * @return
	 */
	public boolean del (int i) {
		try {
			if (i>size) {
				return false;
			}
			if (i==0) {
				return delHead();
			} else {
				//находим предыдущий удаляемому элемент
				PointLink<T> l=traversToLast(i-1);
				PointLink<T> l2=l;
				// перенаправляем его на следующий за удаляемым 
				l.lnk=l.lnk.lnk;
				// обрываем связ удаляемого со списком
				l2.lnk.lnk=null;
				size--;
			}
		} catch (Throwable e) {
			return false;
		}		
		return true;
	}
	
	public boolean delTail () {
		try {
			size--;
			pointTail=traversToLast(size);
		} catch (Throwable e) {
			return false;
		}		
		return true;
	}

	/***
	 * удалить все из списка
	 * просто режем голову size раз - наименнее затратно
	 * @return успех?
	 */
	public boolean clear () {
		try {
			int i;
			int sz=size;
			for (i=0; i<sz; i++) {
				if (!delHead()) {
					break;
				}
			}
			size=sz-i;
			if (size>0) {
				return false;
			}
		} catch (Throwable e) {
			return false;
		}		
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder ss = new StringBuilder();
		ss.append("CustomLinkList [size=" + size + "]\n");
		PointLink<T> l=pointHead;
		int i=0;
		while (l!=null) {
			ss.append(i+": "+l.value+"\n");
			l=l.lnk;
			i++;
		}
		
		return ss.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pointHead == null) ? 0 : pointHead.hashCode());
		result = prime * result + ((pointTail == null) ? 0 : pointTail.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomLinkList other = (CustomLinkList) obj;
		if (pointHead == null) {
			if (other.pointHead != null)
				return false;
		} else if (!pointHead.equals(other.pointHead))
			return false;
		if (pointTail == null) {
			if (other.pointTail != null)
				return false;
		} else if (!pointTail.equals(other.pointTail))
			return false;
		if (size != other.size)
			return false;
		return true;
	}


	/***
	 * 
	 * @author Sergii_Kotov
	 * класс для описания элемента связанного списка
	 * содержит сам элемент и ссылку на следующий элемент. Если следующего нет, то null
	 * @param <E> параметризован
	 */
	private class PointLink <E>	{
		E value;
		PointLink <E> lnk;
		
		public PointLink(E value, PointLink<E> lnk) {
			this.value = value;
			this.lnk = lnk;
		}

		@Override
		public String toString() {
			return "PointLink [value=" + value + ", IsTail=" + (lnk==null?"yes":"no") + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PointLink other = (PointLink) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		private CustomLinkList<T> getOuterType() {
			return CustomLinkList.this;
		}
	}
	
}
